fn main() {
    let tup = (500, 6.4, 1);

    let (x, y, z) = tup;

    println!("The value of y is: {}", y);
    println!("The first value of the tuple is: {}", tup.0);

    let a: [i32; 5] = [1, 2, 3, 4, 5];
    println!("{:?}",a);

    // let a = [3; 5]; // This is the same as writing `let a = [3, 3, 3, 3, 3];`

    let first = a[0];
    let second = a[1];
    println!("First: {}, Second: {}", first, second);

}
