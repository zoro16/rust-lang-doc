#![allow(unused_variables)]
fn main() {
    let mut s = String::new();

    let s = String::from("initial contents");

    let data = "initial contents";
    println!("{}", data);

    let s = data.to_string();
    println!("{}", s);

    // the method also works on a literal directly:
    let mut s = "initial contents".to_string();
    println!("{}", s);

    s.push_str("some extra shit");
    println!("{}", s);

    //============================================================================

    let mut s1 = String::from("foo");
//    s1.push("l");

    let s2 = "bar";

    s1.push_str(s2);
    println!("s2 is {}", s2);
    println!("s1 is {}", s1);
    //============================================================================
    let mut s = String::from("lo");
    s.push('l');
    s.push('l');
    s.push('l');
    s.push('l');

    println!("s is {}", s);
    //============================================================================


    let s1 = String::from("Hello, ");
    let s2 = String::from("world! ");
    let s3 = s1 + &s2;
    println!("s3 is {}", s3);

    //============================================================================

    let hello = "Здравствуйте";
    // let hello = "Hello world";
    let s = &hello[0..6];
    println!("{}", s);
    //============================================================================
    for c in "नमस्ते".chars() {
        println!("{}", c);
    }
    //============================================================================
    for c in "नमस्ते".bytes() {
        println!("{}", c);
    }
    //============================================================================
    // for c in "नमस्ते" {
    //     println!("{}", c);
    // }
    //============================================================================

}
