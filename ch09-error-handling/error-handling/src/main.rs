use std::fs::{self, File};
use std::io::{self, Read, ErrorKind};
use std::net::IpAddr;



fn main() {
    // panic!("crash and burn");
    //=================================================================
    // let v = vec![1, 2, 3];
    // v[99];
    //=================================================================

    // let f = File::open("hello.txt");

    // let f = match f {
    //     Ok(file) => file,
    //     Err(error) => {
    //         panic!("Problem opening the file: {:?}", error)
    //     },
    // };
    // println!("{}", f);
    //=================================================================

    // TOO MUCH `match`, BUT IT WORKS
    // let f = File::open("hello.txt");

    // let f = match f {
    //     Ok(file) => file,
    //     Err(error) => match error.kind() {
    //         ErrorKind::NotFound => match File::create("hello.txt") {
    //             Ok(fc) => fc,
    //             Err(e) => panic!("Problem creating the file: {:?}", e),
    //         },
    //         other_error => panic!("Problem opening the file: {:?}", other_error),
    //     },
    // };
    //=================================================================

    // MORE SEASONED RUSTACAN WOULD WRITE SOMETHING LIKE THIS
    // let f = File::open("hello.txt").unwrap_or_else(|error| {
    //     if error.kind() == ErrorKind::NotFound {
    //         File::create("hello.txt").unwrap_or_else(|error| {
    //             panic!("Problem creating the file: {:?}", error);
    //         })
    //     } else {
    //         panic!("Problem opening the file: {:?}", error);
    //     }
    // });

    //=================================================================
    // let f = File::open("hello.txt").unwrap();
    // println!("{:?}", f);
    //=================================================================

    // let f = File::open("hello.txt").expect("Failed to open hello.txt file.");
    //=================================================================

    // let s = read_username_from_file();
    // println!("{}", s.unwrap());
    //=================================================================


    let home: IpAddr = "127.0.0.1".parse().unwrap();
    println!("{}", home);


    let guess = Guess::new();
    // println!("{}", guess.value);
}

pub struct Guess {
    value: i32,
}

impl Guess {
    pub fn new(value: i32) -> Guess {
        if value < 1 || value > 100 {
            panic!("Guess value must be between 1 and 100, got {}.", value);
        }

        Guess {
            value
        }
    }

    pub fn value(&self) -> i32 {
        self.value
    }
}

// fn read_username_from_file() -> Result<String, io::Error> {
//     let f = File::open("hello.txt");

//     let mut f = match f {
//         Ok(file) => file,
//         Err(e) => return Err(e),
//     };

//     let mut s = String::new();

//     match f.read_to_string(&mut s) {
//         Ok(_) => Ok(s),
//         Err(e) => Err(e),
//     }
// }


// // THE SAME FUNCTIONALITY COULD BE IMPLEMETED LIKE THE FOLLOWING

// fn read_username_from_file() -> Result<String, io::Error> {
//     let mut f = File::open("hello.txt")?;
//     let mut s = String::new();
//     f.read_to_string(&mut s)?;
//     Ok(s)
// }

// // EVEN SIMPLER

// fn read_username_from_file() -> Result<String, io::Error> {
//     let mut s = String::new();

//     File::open("hello.txt")?.read_to_string(&mut s);

//     Ok(s)
// }


// // TO CUTE THE CRAB
// fn read_username_from_file() -> Result<String, io::Error> {
//     fs::read_to_string("hello.txt")
// }
